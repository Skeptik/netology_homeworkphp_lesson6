<?php
require_once __DIR__.'/function.php';
menu();
if (empty($_GET['nameTest'])){
	echo "<center><h1>Тест не выбран</h1></center>";
	die;
}else{
	$nameTest = $_GET['nameTest'];
	if(file_exists(__DIR__."/tests/$nameTest")){
		$arrTest= json_decode(file_get_contents(__DIR__."/tests/$nameTest"), true);
	}else{
		echo "<center><h1>Файл не найден.</h1></center>";
		die;
	}
}
?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Выполнение теста</title>
 	<style type="text/css">
   div { 
    padding: 7px;
    padding-right: 20px; 
    border: solid 1px black;
    font-family: Verdana, Arial, Helvetica, sans-serif; 
    font-size: 13pt; 
   	background: #E6E6FA;
   }
   body{
   	background: #159445;
   }
  </style> 
 </head>
 <body>
 	<h1 align="center"><?= $_GET['nameTest'] ?></h1>
 	<form method="POST">
 	<?php
   	foreach ($arrTest as $key => $value) {
   		echo "<div>";
   		foreach ($value as $key1 => $value1) {
   			if(!is_array($value1) && $key1!='corrected'){
   				echo '<strong>'.$value1.'</strong>'.'<br>';
   			}
   			if(is_array($value1)){
   				foreach ($value1 as $key2 => $value2) {
   					echo '<label><input value='.$key2.' type="radio" name='.$key1.'>'.$value2.'</label>';
   				}	
   			}
   		}
      echo "</div>";
 	  }
 	?>
 	<input name="but" type="submit" value="Выполнить тест">
 	</form>
 </body>
 </html>

 <?php 
 $results = 0;
 if($_POST){
 	if(count($_POST)-1 == count($arrTest)){
 		foreach ($arrTest as $k => $v) {
 			foreach ($v as $k1 => $v2) {
 				if (is_array($v2) && $_POST[$k1] == $arrTest[$k]['corrected']){
 					$results++;
 				}
 			}
 		}
 		echo '<div align="center"><h1 style="color: red">Вы ответили на '.$results.' из '.count($arrTest).' вопросов.</h1></div>';
 	}else{
 		echo '<div align="center"><h1 style="color: red">Выбраны не все ответы.</h1></div>';
 	}
 }
  ?>