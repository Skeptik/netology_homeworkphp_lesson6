<?php
require_once __DIR__.'/function.php';
menu();
if(!empty($_FILES['input']['name'])){
	$fileName = $_FILES['input']['name'];
	$explode = explode('.', $fileName);
	if($explode[1] != 'json'){
		echo "<center><h1>Можно загружать только файл с расширением json.</h1></center>";
		die;
	}
	$arrTest = json_decode(file_get_contents($_FILES['input']['tmp_name']));
	if(!validJson($arrTest)){
		echo "<center><h1>Не верная структура JSON файла.</h1><center>";
		die;
	}
	if (!move_uploaded_file($_FILES['input']['tmp_name'], __DIR__."/tests/$fileName") && !$_FILES['input']['error'] == UPLOAD_ERR_OK){
		echo "<center><h1>Не удалось загрузить файл с тестом.</h1></center>";
		die;
	}
}
$arrName = array_diff(scandir(__DIR__."/tests"), array('..', '.'));
?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
 </head>
 <body>
 	<div align="center">
 	<ul>
 		<?php
 		if(!empty($arrName)){
	 		foreach ($arrName as $key => $value) {?>
	 			<h3><a href="test.php?nameTest=<?= $value ?>"><?= $value ?></a></h3>
 <?php 		
 			}	
		}else{
			echo "<center><h1>Нет загруженных тестов</h1></center>";
		}
 		 ?>
 	</ul>
 	</div>
 </body>
 </html>